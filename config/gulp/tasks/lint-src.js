import gulp from 'gulp';
import lint from '../util/lint';

// Lint the source files
gulp.task('lint:src', () => lint('src/**/*.js'));
