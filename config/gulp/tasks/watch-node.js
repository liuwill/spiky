import gulp from 'gulp';

// Run the headless unit tests as you make changes.
gulp.task('watch:node', () => {
	process.env.NODE_ENV = 'test';
	gulp.watch(['src/**/*', 'test/**/*', 'package.json', '**/.eslintrc'], ['test']);
});