import gulp from 'gulp';
import runSequence from 'run-sequence';
import sequenceComplete from '../util/sequenceComplete';

// Bundle all bundles
gulp.task('build', done => {
	runSequence('lint:src',
		'clean',
		['build:dev', 'build:prod'],
		sequenceComplete(done));
});