import gulp from 'gulp';
import gZip from 'gulp-gzip';
import tar from 'gulp-tar';
import pkg from '../../../package.json';

gulp.task('tarball', () => {
	gulp.src('./dist/*')
		.pipe(tar(pkg.name + '.tar'))
		.pipe(gZip({ threshold: 1024 }))
		.pipe(gulp.dest('./dist'));
});