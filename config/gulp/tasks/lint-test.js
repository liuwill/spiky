import gulp from 'gulp';
import lint from '../util/lint';

// Lint the test files
gulp.task('lint:test', () => lint('test/**/*.js'));